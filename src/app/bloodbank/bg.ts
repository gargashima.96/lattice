export class BloodGroup {
  bloodGroupName: string;
  availabeQuanity: number;
  isBloodGroupActive: boolean;

  constructor(bloodGroupName, availabeQuanity) {
    this.bloodGroupName = bloodGroupName;
    this.availabeQuanity = availabeQuanity;
  }
}

export class Requirement {
  bloodGroupName: string;
  bottlesRequried: number;
}

export class Bucket {
  bloodGroupName: string;
  bottlesRequried: number;
}
