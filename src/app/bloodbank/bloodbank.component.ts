import { Component, OnInit } from '@angular/core';
import { BloodGroup, Requirement, Bucket } from './bg';

@Component({
  selector: 'app-bloodbank',
  templateUrl: './bloodbank.component.html',
  styleUrls: ['./bloodbank.component.css']
})
export class BloodbankComponent implements OnInit {
  bloodGroupList: Array<BloodGroup>;
  requirement: Requirement;
  bucket: Requirement;

  tempBloodBankIndex: number;
  constructor() {}

  ngOnInit() {
    this.bloodGroupList = new Array<BloodGroup>();
    this.bloodGroupList.push(new BloodGroup('A+', 10));
    this.bloodGroupList.push(new BloodGroup('B+', 10));
    this.bloodGroupList.push(new BloodGroup('O+', 10));
    this.bloodGroupList.push(new BloodGroup('AB+', 10));
    this.bloodGroupList.push(new BloodGroup('A-', 10));
    this.bloodGroupList.push(new BloodGroup('B-', 10));
    this.bloodGroupList.push(new BloodGroup('O-', 10));
    this.bloodGroupList.push(new BloodGroup('AB-', 10));

    this.requirement = new Requirement();
    this.requirement.bloodGroupName = 'A+';
  }

  onBloodReqChange() {
    this.bucket = undefined;
    this.bloodGroupList.forEach(element => {
      element.isBloodGroupActive = false;
    });
    this.checkActive();
  }

  checkActive() {
    const element = this.bloodGroupList.find(
      item => item.bloodGroupName === this.requirement.bloodGroupName
    );
    if (element.availabeQuanity > this.requirement.bottlesRequried) {
      element.isBloodGroupActive = true;
    }
  }

  onReqQuantityChange() {
    this.bucket = undefined;
    this.bloodGroupList.forEach(element => {
      element.isBloodGroupActive = false;
    });
    this.checkActive();
  }

  dragStart(event, i) {
    this.tempBloodBankIndex = i;
  }

  dropEvent(event) {
    event.preventDefault();
    this.bucket = new Bucket();
    const index = this.tempBloodBankIndex;
    this.bucket.bloodGroupName = this.bloodGroupList[index].bloodGroupName;
    this.bloodGroupList[index].availabeQuanity =
      this.bloodGroupList[index].availabeQuanity -
      this.requirement.bottlesRequried;
    this.bucket.bottlesRequried = this.requirement.bottlesRequried;
  }

  dragOverEvent(event) {
    event.preventDefault();
  }
}
