import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BloodbankComponent } from './bloodbank/bloodbank.component';

const routes: Routes = [{ path: '', component: BloodbankComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
